({
    // followRecord Method used to Follow/Unfollow a record
    followRecord : function(component) {
        var recordId = component.get("v.recordId");
        var isFollowed = component.get("v.isFollowed");
        var action=component.get("c.followRecord");
        action.setParams({
            "recordId" : component.get("v.recordId"),
            "isFollowed" : component.get("v.isFollowed")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS")
            {
                component.set("v.isFollowed",response.getReturnValue())
            }
            else if(state === "ERROR")
            {
                alert('Problem with connection. Please try again.');
            }
        });
        $A.enqueueAction(action);
    }
})
&amp;lt;span&amp;gt;