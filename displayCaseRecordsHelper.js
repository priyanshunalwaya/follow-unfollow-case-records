({
    getCaseRecords : function(component) {
        var action=component.get("c.getCaseRecords");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS"){
                component.set("v.caseWrapList",response.getReturnValue());
            }
            else if(state === "ERROR"){
                alert('Problem with connection. Please try again.');
            }
        });
        $A.enqueueAction(action);
    }
})