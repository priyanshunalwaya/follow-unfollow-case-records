({
    doInit : function(component, event, helper) {
        helper.getCaseRecords(component);
    },
    navigateTOCaseRecord : function(component, event, helper) {
        var recordId=event.getSource().get("v.value");
        window.open('/' + recordId);
    }
})